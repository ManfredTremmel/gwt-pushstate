/*
 * Copyright 2014 Richard Wallis
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.wallissoftware.pushstate.client;

import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.place.shared.PlaceHistoryHandler.Historian;
import com.google.gwt.user.client.Window;

import elemental2.dom.DomGlobal;
import elemental2.dom.PopStateEvent;
import elemental2.dom.Window.OnpopstateFn;

import org.apache.commons.lang3.StringUtils;

public class PushStateHistorianImpl implements Historian, HasValueChangeHandlers<String> {

  private final EventBus handlers = new SimpleEventBus();
  private String token;
  private final String relativePath;

  /**
   * constructor.
   *
   * @param prelativePath relative path to use
   */
  PushStateHistorianImpl(final String prelativePath) {
    relativePath = StringUtils.endsWith(prelativePath, "/") ? prelativePath
        : StringUtils.defaultString(prelativePath) + "/";
    initToken();
    registerPopstateHandler();
  }

  @Override
  public final String getToken() {
    return token;
  }

  @Override
  public void newItem(final String ptoken, final boolean pissueEvent) {
    this.newItem(ptoken, pissueEvent, false);
  }

  /**
   * add new item.
   *
   * @param ptoken token of the page
   * @param pissueEvent issue event
   * @param preplaceState replace state
   */
  public void newItem(final String ptoken, final boolean pissueEvent, final boolean preplaceState) {
    if (setToken(ptoken)) {
      if (preplaceState) {
        PushStateHistorianImpl.replaceState(relativePath, token);
      } else {
        PushStateHistorianImpl.pushState(relativePath, token);
      }

      if (pissueEvent) {
        ValueChangeEvent.fire(this, token);
      }
    }
  }

  @Override
  public void fireEvent(final GwtEvent<?> pevent) {
    handlers.fireEvent(pevent);
  }

  private void registerPopstateHandler() {
    final OnpopstateFn oldHandler = DomGlobal.window.onpopstate;
    DomGlobal.window.onpopstate = pevt -> {
      if (pevt instanceof PopStateEvent) {
        final Object stateJason = ((PopStateEvent) pevt).state;
        if (stateJason instanceof String) {
          onPopState((String) stateJason);
        }
        if (oldHandler != null) {
          oldHandler.onInvoke(pevt);
        }
      }
      return null;
    };
  }

  private void onPopState(final String ptoken) {
    if (setToken(ptoken)) {
      ValueChangeEvent.fire(this, getToken());
    }
  }

  private void initToken() {
    final String token = Window.Location.getPath() + Window.Location.getQueryString();
    setToken(token);
    PushStateHistorianImpl.replaceState(relativePath, getToken());
  }

  private String stripStartSlash(final String pinput) {
    return StringUtils.removeStart(pinput, "/");
  }

  private String stripRelativePath(final String ptoken) {
    final String relPath = stripStartSlash(relativePath);
    final String token = stripStartSlash(ptoken);

    if (StringUtils.startsWith(token, relPath)) {
      return stripStartSlash(StringUtils.substring(token, relPath.length()));
    }
    return token;
  }

  private static void replaceState(final String prelativePath, final String ptoken) {
    DomGlobal.window.history.replaceState(ptoken, DomGlobal.document.title, prelativePath + ptoken);
  }

  private static void pushState(final String prelativePath, final String ptoken) {
    DomGlobal.window.history.pushState(ptoken, DomGlobal.document.title, prelativePath + ptoken);
  }

  private boolean setToken(final String pnewToken) {
    final String newToken = stripRelativePath(pnewToken);
    if (!matchesToken(newToken)) {
      token = newToken;
      return true;
    }
    return false;
  }

  private boolean matchesToken(final String pcompare) {
    return StringUtils.equals(pcompare, token) || StringUtils.equals(pcompare, token + "/")
        || StringUtils.equals(token, pcompare + "/");
  }

  @Override
  public HandlerRegistration addValueChangeHandler(
      final ValueChangeHandler<String> pvalueChangeHandler) {
    return handlers.addHandler(ValueChangeEvent.getType(), pvalueChangeHandler);
  }
}
